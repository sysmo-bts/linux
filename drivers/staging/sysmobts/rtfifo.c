#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>
#include <linux/completion.h>
#include <linux/freezer.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/dspdl-coff.h>
#include <linux/slab.h>

#include <mach/irqs.h>
#include <mach/hardware.h>
#include <mach/common.h>
#include <mach/mux.h>

#include <asm/mach-types.h>
#include <asm/gpio.h>
#include <asm/io.h>

#ifdef CONFIG_PROC_FS
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#endif /* CONFIG_PROC_FS */

#define MODULE_NAME 	"rtfifo"

#define RTFIFO_DSP_INTERRUPT	0

/* including davinci.h is difficult */
#define DAVINCI_SYSTEM_MODULE_BASE	0x01c40000

static const char rtfifo_driver_version[] = "v1.1";

static char *rtfifo_fw_name = "dsp.out";
module_param_named(fw_name, rtfifo_fw_name, charp, 0644);

static int rtfifo_arm_irq = 46;
module_param_named(arm_irq, rtfifo_arm_irq, int, S_IRUGO);
MODULE_PARM_DESC(arm_irq, "ARM-side FIFO interrupt number. Default: 46");

struct rtfifo_t {
	u32  u32MagicId;
	u32  u32Size;
	u32  reserved[2];
	u32  u32Head;
	u32  u32Tail;
	u8   u8Dir;
	u8   u8ArmIntReq;
	u8   u8ArmIntFlag;
	u8   u8DspIntReq;
	u8   u8DspIntFlag;
	char szName[35];
	u8   pu8Data[0];
} rtfifo_t;

struct rtfifo_mdev_t {
	char devName[128];
 	struct mutex rd_lock; 
 	struct mutex wr_lock; 
	struct miscdevice dev;

  	spinlock_t lock;
	wait_queue_head_t waitq;
	volatile struct rtfifo_t __iomem *rtfifo;
};

struct rtfifo_dev_t {
	int irq;
	int wake;
	
	u32 mdev_count;	
	struct rtfifo_mdev_t *mdev;

    	volatile void * __iomem *mmio;
};

static struct rtfifo_dev_t *rtfifo_dev;

static int rtfifo_TrigDspIsr( int intNum )
{
	void __iomem *intgen = IO_ADDRESS(DAVINCI_SYSTEM_MODULE_BASE + 0x10);

	__raw_writel(0x30F10000 | (1 << (intNum + 4)), intgen);
	
	return 0;
}

static irqreturn_t rtfifo_isr(int irq, void *dev_id)
{
	int i;
	struct rtfifo_dev_t *dev = dev_id;
	
	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].rtfifo->u8ArmIntFlag) {
			dev->mdev[i].rtfifo->u8ArmIntReq = 0;
			dev->mdev[i].rtfifo->u8ArmIntFlag = 0;
			wake_up_interruptible(&dev->mdev[i].waitq);
		}
    	}
	return IRQ_HANDLED;
}

#ifdef CONFIG_PROC_FS
static void *rtfifo_seq_start(struct seq_file *m, loff_t *pos)
{
	return *pos < 1 ? (void *)1 : NULL;
}

static void *rtfifo_seq_next(struct seq_file *m, void *v, loff_t *pos)
{
	++*pos;
	return NULL;
}

static void rtfifo_seq_stop(struct seq_file *m, void *v)
{
}

static int rtfifo_proc_show(struct seq_file *m, void *v)
{
	int i;
	struct rtfifo_dev_t *dev = m->private;

	for (i = 0; i < dev->mdev_count; i++) {

		seq_printf(m, "\nRTFIFO #%d/%d\n===========\n", i+1, dev->mdev_count); 
		seq_printf(m, "\tname: %s\n", dev->mdev[i].rtfifo->szName);
		seq_printf(m, "\tdir : %s\n", dev->mdev[i].rtfifo->u8Dir ? "ARM -> DSP" : "DSP -> ARM");
		seq_printf(m, "\tsize: %d bytes\n", dev->mdev[i].rtfifo->u32Size);
		seq_printf(m, "\thead: %d\n", dev->mdev[i].rtfifo->u32Head);
		seq_printf(m, "\ttail: %d\n", dev->mdev[i].rtfifo->u32Tail);
		seq_printf(m, "\tirq : ARM %d:%d  DSP %d:%d\n", dev->mdev[i].rtfifo->u8ArmIntReq,
			dev->mdev[i].rtfifo->u8ArmIntFlag, dev->mdev[i].rtfifo->u8DspIntReq, 
			dev->mdev[i].rtfifo->u8DspIntFlag );
	}	
	return 0;
}

static const struct seq_operations rtfifo_proc_op = {
	.start	= rtfifo_seq_start,
	.next	= rtfifo_seq_next,
	.stop	= rtfifo_seq_stop,
	.show	= rtfifo_proc_show
};

static int rtfifo_proc_open(struct inode *inode, struct file *file)
{
	int ret;
	struct seq_file *m;
	struct rtfifo_dev_t *dev;

	dev = PDE_DATA(inode);
	if (!dev)
		return -ENOENT;

	ret = seq_open(file, &rtfifo_proc_op);
	if (ret < 0)
		return ret;

	m = file->private_data;
	m->private = dev;

	return 0;
}

static ssize_t rtfifo_proc_write(struct file *file, const char __user *buf,
			       size_t size, loff_t *ppos)
{
	char *kbuf;
	int ret = 0;

	if (size <= 1 || size >= PAGE_SIZE)
		return -EINVAL;

	kbuf = kmalloc(size + 1, GFP_KERNEL);
	if (!kbuf)
		return -ENOMEM;

	if (copy_from_user(kbuf, buf, size) != 0) {
		kfree(kbuf);
		return -EFAULT;
	}
	kbuf[size] = 0;

	if (0 == 1) {
	}
	else {
		printk(KERN_ERR "rtfifo: Invalid Command (%s)\n", kbuf);
		kfree(kbuf);
		return -EINVAL;
	}

	ret = size;

	kfree(kbuf);
	return ret;
}

static const struct file_operations rtfifo_proc_fops = {
	.open		= rtfifo_proc_open,
	.write		= rtfifo_proc_write,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
	.owner		= THIS_MODULE,
};

#endif /* CONFIG_PROC_FS */

static int rtfifo_open(struct inode *inode, struct file *filp)
{
	int i;
	struct rtfifo_mdev_t *mdev = NULL;

	for (i = 0; i < rtfifo_dev->mdev_count; i++) {

		if (rtfifo_dev->mdev[i].dev.minor == MINOR(inode->i_rdev)) {
			mdev = &rtfifo_dev->mdev[i];
			break;
		}
	}
	if (mdev == NULL) {
		return -ENODEV;
	}

	filp->private_data = mdev;
	return 0;
}

static int rtfifo_release(struct inode *inode, struct file *filp)
{
	//struct rtfifo_mdev_t *mdev = filp->private_data;

	return 0;
}

static int rtfifo_read(struct file *filp, char *buf, size_t count, loff_t *ppos)
{
	int n;
	int ret;
	wait_queue_t wq;
	struct rtfifo_mdev_t *mdev = filp->private_data;

	mutex_lock(&mdev->rd_lock);
	while (mdev->rtfifo->u32Tail == mdev->rtfifo->u32Head) {

		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&mdev->rd_lock);
			return -EAGAIN;  
		}

		init_waitqueue_entry( &wq, current );

		/* Ask the DSP to be interrupted */	
		mdev->rtfifo->u8ArmIntReq = 1;

		ret = wait_event_interruptible(mdev->waitq,
					       mdev->rtfifo->u32Tail != mdev->rtfifo->u32Head);
		if (ret < 0) {
			mutex_unlock(&mdev->rd_lock);
			return ret;
		}
	}

	for (n=0; n<count;) {
		u32 tail = mdev->rtfifo->u32Tail;
		u32 head = mdev->rtfifo->u32Head;

        	/* If no data available ... */
		if (tail == head) {
			/* return */
			mutex_unlock(&mdev->rd_lock);
			return n;
		}

        	if (tail > head) {
            		if ( (count-n) <= (mdev->rtfifo->u32Size - tail) ) {
				ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], count-n);
			    	if (ret) {  
					mutex_unlock(&mdev->rd_lock);
					return ret;  
				}	
				mdev->rtfifo->u32Tail = tail + (count - n);
				n += (count - n);
			} else {
				ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], 
									mdev->rtfifo->u32Size-tail);  
				if (ret) {  
					mutex_unlock(&mdev->rd_lock);
					return ret;  
				}	
				mdev->rtfifo->u32Tail = 0;
				n += (mdev->rtfifo->u32Size - tail);
			}
		} else {
            		if ( (count-n) < (head-tail) ) {
        		    	ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], count-n );
                		mdev->rtfifo->u32Tail = tail + (count - n);
                		n += (count - n);
            		} else {
        		    	ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], head - tail );
                		mdev->rtfifo->u32Tail = head;
                		n += (head - tail);
            		}
        	}

    	}
	mutex_unlock(&mdev->rd_lock);
    
	// Verify if the DSP wants to be interrupted                              
	if ( n && mdev->rtfifo->u8DspIntReq ) {

		// Interrupt the DSP
		mdev->rtfifo->u8DspIntFlag = 1;
		rtfifo_TrigDspIsr( RTFIFO_DSP_INTERRUPT );
	}    

	return n;
}
 
static int rtfifo_write(struct file *filp, const char *buf, 
				size_t count, loff_t *ppos)
{
	int n;
	int ret;
	uint32_t head;
	uint32_t tail;
	uint32_t nextHead;
	wait_queue_t wq;
	struct rtfifo_mdev_t *mdev = filp->private_data;

	mutex_lock(&mdev->wr_lock);

	head = mdev->rtfifo->u32Head;
	nextHead = ((head+1) >= mdev->rtfifo->u32Size) ? 0 : (head+1);
    
	// Look if there is room available
	while (nextHead == mdev->rtfifo->u32Tail) {
		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&mdev->wr_lock);
			return -EAGAIN;  
		}

		init_waitqueue_entry( &wq, current );

		/* Ask the DSP to be interrupted */	
		mdev->rtfifo->u8ArmIntReq = 1;

		ret = wait_event_interruptible(mdev->waitq,
					       nextHead != mdev->rtfifo->u32Tail);
		if (ret < 0) {
			mutex_unlock(&mdev->wr_lock);
			return ret;
		}
	}

	for (n=0; n<count;) {
		tail = mdev->rtfifo->u32Tail;   
		head = mdev->rtfifo->u32Head;
		nextHead = ((head+1) >= mdev->rtfifo->u32Size) ? 0 : (head+1);
   
		/* If no data available ... */
		if (nextHead == tail) {
			/* return */
			mutex_unlock(&mdev->wr_lock);
			return n;
		}

		if ( head >= tail ) {
			if ((count-n) <= (mdev->rtfifo->u32Size - head)) {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], count-n)) {  
				    	mutex_unlock(&mdev->wr_lock);
				       	return -EFAULT;  
				}
				mdev->rtfifo->u32Head = head + (count - n);
				n += (count - n);
			} else {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], 
									mdev->rtfifo->u32Size-head)) {
					mutex_unlock(&mdev->wr_lock);
					return -EFAULT;  
				}
				mdev->rtfifo->u32Head = 0;
				n += (mdev->rtfifo->u32Size - head);
			}
		} else {
			if ((count-n) < (tail-head)) {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], count-n)) {  
				    	mutex_unlock(&mdev->wr_lock);
				       	return -EFAULT;  
				}
				mdev->rtfifo->u32Head = head + (count - n);
				n += (count - n);
			} else {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], (tail - head) - 1)) {  
				    	mutex_unlock(&mdev->wr_lock);
				       	return -EFAULT;  
				}
				mdev->rtfifo->u32Head = tail - 1;
				n += (tail - head) - 1;
			}
		}
	}
   	mutex_unlock(&mdev->wr_lock);
    
	// Verify if the DSP wants to be interrupted                              
	if ( n && mdev->rtfifo->u8DspIntReq ) {

		// Interrupt the DSP
		mdev->rtfifo->u8DspIntFlag = 1;
		rtfifo_TrigDspIsr( RTFIFO_DSP_INTERRUPT );
	}    

	return n;
}

static const struct file_operations rtfifo_fops = {
	.open		= rtfifo_open,
	.release	= rtfifo_release,
	.read		= rtfifo_read,
	.write		= rtfifo_write,
	.owner		= THIS_MODULE,
};

static int rtfifo_probe(struct platform_device *pdev)
{
	int i;
	int ret = -ENODEV;
	struct rtfifo_t *pFifo;
	struct file_hdr_t *file_hdr;
	struct opt_hdr_t *opt_hdr;
	struct section_hdr_t *section_hdr;
	const struct firmware *fw_entry;
	struct rtfifo_dev_t *dev = NULL;
	struct proc_dir_entry *ent[2];

	ret = request_firmware( &fw_entry, rtfifo_fw_name, &pdev->dev);
	if (ret < 0) {
		printk( KERN_ERR "rtfifo: firmware <%s> not available\n", rtfifo_fw_name);
		goto error_fw_request;
	}

	/* The memory is set to zero by kzalloc */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		printk(KERN_ERR "rtfifo: Failed to allocate device structure\n");
		ret = -ENOMEM;
		goto error_alloc_dev;
	}

	/* Extract header infos. */
	file_hdr = (struct file_hdr_t *)fw_entry->data;
	if (file_hdr->magic != 0xC2) {
		printk(KERN_ERR "rtfifo: Invalid magic number in header (0x%02X)\n",
			file_hdr->magic);
		ret = -EFAULT;
		goto error_fw;
	}
	if (file_hdr->target_id != 0x0099) {
		printk(KERN_ERR "rtfifo: Invalid target ID in header (0x%04X)\n",
			file_hdr->target_id);
		ret = -EFAULT;
		goto error_fw;
	}

	/* Make sure file is a valid COFF. */
	if (!(file_hdr->flags & F_EXEC)) {
		printk(KERN_ERR "rtfifo: Not an executable");
		ret = -EFAULT;
		goto error_fw;
	}

	/* Points to first Section */
	opt_hdr = (struct opt_hdr_t *) (fw_entry->data + sizeof(struct file_hdr_t));
	section_hdr = (struct section_hdr_t *)((void *) opt_hdr + file_hdr->opthdr);

	/* Try to locate the RTFIFO section */
	for (i = 0; i < file_hdr->nscns; i++) {
		if ((section_hdr->s_size != 0) &&
		    (section_hdr->s_scnptr != 0) &&
		    !(section_hdr->s_flags & STYP_COPY) &&
		    (strncmp(section_hdr->s_name, "RTFIFO", 6) == 0)) {
			/* Found */
			break;
		}
		section_hdr++;
	}
	if (i == file_hdr->nscns) {
		printk(KERN_ERR "rtfifo: No \"RTFIFO\" section found in COFF file\n");
		ret = -EFAULT;
		goto error_fw;
	}

	/* Map RTFIFO memory section */
    	dev->mmio = ioremap(section_hdr->s_paddr, section_hdr->s_size);
    	if (!dev->mmio) {        
		printk(KERN_ERR "rtfifo: Failed to I/O remap RTFIFO memory\n");
        	ret = -ENOMEM;
        	goto error_ioremap;
    	}

	/* Count the number of fifos */
	pFifo = (struct rtfifo_t *)dev->mmio;
	for (i = sizeof(struct rtfifo_t); i < section_hdr->s_size; ) {
		if ((pFifo->u32MagicId != 0x4649464F/*'FIFO'*/) || 
		    (i + pFifo->u32Size > section_hdr->s_size)) {
			break;
		}
		dev->mdev_count++;
		i += (sizeof(struct rtfifo_t) + pFifo->u32Size + 0x3F) & ~0x3F;
		pFifo = (struct rtfifo_t *)((u32)pFifo + ((sizeof(struct rtfifo_t) + pFifo->u32Size + 0x3F) & ~0x3F));
	}
	if (dev->mdev_count == 0) {
		printk(KERN_ERR "rtfifo: No valid rtfifo found\n");
		ret = -EFAULT;
		goto error_nofifo;
	}
	release_firmware(fw_entry);

	/* Alloc memory for each fifo misc device */
	dev->mdev = kzalloc(dev->mdev_count * sizeof(struct rtfifo_mdev_t), GFP_KERNEL);
    	if (!dev->mdev) {
		printk(KERN_ERR "rtfifo: Failed to allocate misc device structure\n");
		ret = -ENOMEM;
		goto error_alloc_mdev;
	}

	/* Init each fifo device */
	pFifo = (struct rtfifo_t *)dev->mmio;
	for (i = 0; i < dev->mdev_count; i++) {

		sprintf(dev->mdev[i].devName, "rtfifo!%s", pFifo->szName );
		dev->mdev[i].dev.name  = dev->mdev[i].devName;
		dev->mdev[i].dev.minor = MISC_DYNAMIC_MINOR;
		dev->mdev[i].dev.fops  = &rtfifo_fops;
		ret = misc_register(&dev->mdev[i].dev);
		if (ret < 0) {
			printk(KERN_ERR "rtfifo: Error registering misc driver\n");
			goto error_miscdev;
		}

   		mutex_init(&dev->mdev[i].wr_lock);
    		mutex_init(&dev->mdev[i].rd_lock);
    		spin_lock_init(&dev->mdev[i].lock);
		init_waitqueue_head(&dev->mdev[i].waitq);

		dev->mdev[i].rtfifo = pFifo;
		pFifo = (struct rtfifo_t *)((u32)pFifo + ((sizeof(struct rtfifo_t) + pFifo->u32Size + 0x3F) & ~0x3F));
	}
	
	/* Set the global device structure pointer */
	rtfifo_dev = dev;

	/* Register IRQ */
    	if (request_irq(rtfifo_arm_irq, rtfifo_isr, IRQF_TRIGGER_RISING, MODULE_NAME, dev) < 0) {
        	printk(KERN_ERR "rtfifo: failed to acquire irq %d\n", dev->irq);
        	goto error_irq;
    	}

	/* Create PROC Entry */
	#ifdef CONFIG_PROC_FS
	{
		//ent[0] = proc_mkdir( "dsp", NULL );
		//if (!ent[0]) {
		//	printk(KERN_ERR "rtfifo: failed to create proc entry\n");
		//}
		//ent[1] = create_proc_entry("dsp/"MODULE_NAME, 0, NULL);
		ent[1] = proc_create_data(MODULE_NAME, 0, NULL, &rtfifo_proc_fops, dev);
		if (!ent[1]) {
			printk(KERN_ERR "rtfifo: failed to create proc entry\n");
		}
	}	
	#endif

	printk(KERN_INFO "rtfifo: probed\n");
	return 0;

error_irq:
	rtfifo_dev = NULL;
error_miscdev:
	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].rtfifo != NULL) {
			misc_deregister(&dev->mdev[i].dev);
		}
	}
	kfree(dev->mdev);
error_alloc_mdev:
error_nofifo:
    	iounmap(dev->mmio);
error_ioremap:
error_fw:
	kfree(dev);
error_alloc_dev:
	release_firmware(fw_entry);
error_fw_request:
	printk(KERN_ERR "rtfifo: probe failed\n");
	return ret;
}

static int rtfifo_remove(struct platform_device *pdev)
{
	int i;
	struct rtfifo_dev_t *dev = rtfifo_dev;

	#ifdef CONFIG_PROC_FS
	{
		remove_proc_entry(MODULE_NAME, NULL);
		//remove_proc_entry("dsp/"MODULE_NAME, NULL);
		//remove_proc_entry("dsp", NULL);
	}
	#endif

	disable_irq(rtfifo_arm_irq);
	free_irq(rtfifo_arm_irq, dev);

	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].rtfifo != NULL) {
			misc_deregister(&dev->mdev[i].dev);
		}
	}
	kfree(dev->mdev);
    	iounmap(dev->mmio);
	kfree(dev);
	rtfifo_dev = NULL;
	return 0;
}

static void rtfifo_platform_release(struct device *device)
{
}

static struct platform_driver rtfifo_driver = {
	.probe = rtfifo_probe,
	.remove =rtfifo_remove,
	.driver = {
		.name = MODULE_NAME,
		.owner = THIS_MODULE,
	},
};

static struct platform_device rtfifo_pdevice = {
	.name 	= MODULE_NAME,
	.id 	= 1,
	.dev = {
		.release = rtfifo_platform_release,
	}
};

static int __init rtfifo_init(void)
{
	int err = 0;

	err =  platform_driver_register(&rtfifo_driver);
	if ( err <  0 ) {
		printk( KERN_ERR "Failed to register rtfifo driver\n" );
		return err;
	}

	err = platform_device_register( &rtfifo_pdevice );
	if ( err < 0 ) {
		platform_driver_unregister( &rtfifo_driver );
		printk( KERN_ERR "Failed to register rtfifo driver\n" );
		return err;
	}
	return err;
}

static void __exit rtfifo_cleanup(void)
{
	platform_device_unregister( &rtfifo_pdevice );
	platform_driver_unregister( &rtfifo_driver );
}

module_init(rtfifo_init);
module_exit(rtfifo_cleanup);

MODULE_DESCRIPTION("RT-FIFO Driver");
MODULE_AUTHOR("Lyrtech RD Inc. <www.lyrtech.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" MODULE_NAME);

