#pragma once

#define MAC_ADDR_LEN 6

#define SOBJB_EE_MAGIC		0x50b0d0ee
#define SOBJB_EE_VERSION	1

#define SOB_ODU_V1		1
#define SOB_ODU_V2		2
#define SOB_ODU_V3		3

struct sob_odu_eeprom {
	uint32_t magic;		/* SOBJB_EE_MAGIC */
	uint32_t crc32;		/* crc32_le */

	uint16_t len;		/* sizeof(struct sob_odu_eeprom) */
	uint8_t version;	/* SOBJB_EE_VERSION */
	uint8_t model;		/* SOB_ODU_V* */

	uint32_t serial;
	uint32_t manuf_date;
	uint32_t hw_options;

	uint8_t mac_eth0[MAC_ADDR_LEN];
	uint8_t mac_eth1[MAC_ADDR_LEN];
	uint8_t mac_wlan0[MAC_ADDR_LEN];
	uint8_t mac_wlan1[MAC_ADDR_LEN];
} __attribute__ ((packed));
