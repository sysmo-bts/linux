/*
 * COFF file format definitions.
 *
 * Copyright (C) 2008 Lyrtech <www.lyrtech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef COFF_H
#define COFF_H

/* COFF file header */
struct __attribute__((packed)) file_hdr_t {
	u16	magic;		/* Magic number */
	u16	nscns;		/* Number of sections */
	s32	timdat;		/* Time & date stamp */
	s32	symptr;		/* File pointer to symtab */
	s32	nsyms;		/* Number of symtab entries */
	u16	opthdr;		/* sizeof(optional hdr) */
	u16	flags;		/* Flags */
	u16	target_id;	/* Target architecture ID */
};

/* File header flags */
#define F_RELFLG	0x01	/* Relocation info stripped from file */
#define F_EXEC		0x02	/* file is executable (no unresolved refs) */

/* Optional file header */
struct __attribute__((packed)) opt_hdr_t {
	s16	magic;		/* optional file header magic number */
	s16	vstamp;		/* version stamp */
	s32	tsize;		/* text size in bytes, padded to FW bdry */
	s32	dsize;		/* initialized data "  " */
	s32	bsize;		/* uninitialized data "   " */
	s32	entrypt;	/* Entry point. */
	s32	text_start;	/* Base of text used for this file */
	s32	data_start;	/* Base of data used for this file */
};

struct __attribute__((packed)) section_hdr_t {
	char    s_name[8];	/* section name */
	s32	s_paddr;	/* physical address, aliased s_nlib */
	s32	s_vaddr;	/* virtual address */
	s32	s_size;		/* section size */
	s32	s_scnptr;	/* file ptr to raw data for section */
	s32	s_relptr;	/* file ptr to relocation */
	s32	s_lnnoptr;	/* file ptr to gp histogram */
	u32	s_nreloc;	/* number of relocation entries */
	u32	s_nlnno;	/* number of gp histogram entries */
	u32	s_flags;	/* flags */
	s16	s_reserved;	/* reserved 2 bytes */
	u16	s_page;		/* memory page id */
};

/*------------------------------------------------------------------------*/
/* The low 8 bits of s_flags is used as a section "type"                  */
/*------------------------------------------------------------------------*/
#define STYP_REG    0x00  /* "regular" : allocated, relocated, loaded     */
#define STYP_DSECT  0x01  /* "dummy"   : !allocated, relocated, !loaded   */
#define STYP_NOLOAD 0x02  /* "noload"  : allocated, relocated, !loaded    */
#define STYP_GROUP  0x04  /* not used                                     */
#define STYP_PAD    0x08  /* not used                                     */
#define STYP_COPY   0x10  /* "copy"    : used for C init tables -
			   * not allocated, relocated, loaded;
			   * reloc & lineno entries processed normally    */
#define STYP_TEXT   0x20  /* section contains text only                   */
#define STYP_DATA   0x40  /* section contains data only                   */
#define STYP_BSS    0x80  /* section contains bss only                    */

#endif /* COFF_H */
