/*
 * FPGA bitstream load header file.
 *
 * Copyright (C) 2008 Lyrtech <www.lyrtech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef FPGADL_H
#define FPGADL_H 1

#include <linux/miscdevice.h>

/* FPGA device-specific informations and functions. */
struct fpgadl_pdata_t {
	size_t bitstream_max_size;
	u8 program_b;
	u8 init_b;
	u8 done;
	u8 busy;
	void (*setup)(void);
	void (*teardown)(void);
};

struct fpgadl_driver {
	char *version;
	struct module *module;
	struct device_driver driver;
	struct driver_attribute version_attr;
};

struct fpgadl_device {
	int id;
	char *name;
	enum {
		FPGADL_DEV_STATE_START,
		FPGADL_DEV_STATE_DEVICE_REGISTERED,
		FPGADL_DEV_STATE_GPIO_REGISTERED,
		FPGADL_DEV_STATE_CHAR_DEV_REGISTERED,
	} state;
	int fw_buffer_allocated;
	char *fw_name;
	u8 *fw_data;
	size_t fw_length;
	size_t fw_max_size;
	int bitstream_mode;
	int fw_loaded;
	int (*write_byte)(struct fpgadl_device *, u8 *, int);
	void *devdata; /* Pointer to interface-specific (SPI/PAR) device */
	struct miscdevice miscdev;
	struct fpgadl_driver *driver;
	struct fpgadl_pdata_t *pdata;
	struct device dev;
};

/* Bitstream types. */
#define BITSTREAM_MODE_UNKNOWN	0
#define BITSTREAM_MODE_FULL	1
#define BITSTREAM_MODE_PARTIAL	2

extern struct bus_type fpgadl_bus_type;

#define to_fpgadl_driver(drv) container_of(drv, struct fpgadl_driver, driver);

extern int fpgadl_register_driver(struct fpgadl_driver *drv);
extern void fpgadl_unregister_driver(struct fpgadl_driver *drv);

extern int fpgadl_register_device(struct fpgadl_device *fpgadldev);
extern void fpgadl_unregister_device(struct fpgadl_device *fpgadldev);

int fpgadl_is_bitstream_loaded(const char *name);

#endif /* FPGADL_H */
