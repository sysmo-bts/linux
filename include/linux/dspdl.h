/*
 * DSP firmware loader header file.
 *
 * Copyright (C) 2008 Lyrtech <www.lyrtech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef DSPDL_H
#define DSPDL_H 1

#include <linux/miscdevice.h>

struct dspdl_driver {
	char *version;
	struct module *module;
	struct device_driver driver;
	struct driver_attribute version_attr;
};

struct dspdl_device {
	int id;
	char *name;
	enum {
		DSPDL_DEV_STATE_START,
		DSPDL_DEV_STATE_DEVICE_REGISTERED,
		DSPDL_DEV_STATE_CHAR_DEV_REGISTERED,
	} state;
	int fw_buffer_allocated;
	char *fw_name;
	u8 *fw_data;
	size_t fw_length;
	int fw_loaded;
	int (*cb_start)(struct dspdl_device *);
	int (*cb_finish)(struct dspdl_device *, u32);
	int (*cb_read_mem)(struct dspdl_device *, u32, u32 *);
	int (*cb_write_mem)(struct dspdl_device *, u32, u32);
	int (*cb_write_section)(struct dspdl_device *, u32, u8 *, ssize_t);
	void *devdata; /* Pointer to interface-specific (SPI/PAR) device */
	struct miscdevice miscdev;
	struct dspdl_driver *driver;
	struct device dev;
};

extern struct bus_type dspdl_bus_type;

#define to_dspdl_driver(drv) container_of(drv, struct dspdl_driver, driver);

extern int dspdl_register_driver(struct dspdl_driver *drv);
extern void dspdl_unregister_driver(struct dspdl_driver *drv);

extern int dspdl_register_device(struct dspdl_device *dspdldev);
extern void dspdl_unregister_device(struct dspdl_device *dspdldev);

int dspdl_is_firmware_loaded(const char *name);

#endif /* DSPDL_H */
